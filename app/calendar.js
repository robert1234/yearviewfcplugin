import $ from 'jquery';
import 'jquery-ui';

import Vue from 'vue';
import { Calendar } from "@fullcalendar/core";
import interactionPlugin from "@fullcalendar/interaction";
import dayGridPlugin from "@fullcalendar/daygrid";
import TimeGrid from "@fullcalendar/timegrid";
