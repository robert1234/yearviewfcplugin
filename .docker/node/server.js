'use strict';

import '/var/www/app/calendar.js';
import express from 'express';

// Constants
const PORT = 8080;

// App
const app = express();
app.use(express.static(__dirname + '/public'));
app.get('/', (req, res) => {
    res.send('Hello World');
});

app.listen(PORT);
console.log("Server running...");
console.log(`Running on ${PORT}`);